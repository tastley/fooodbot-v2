var env = process.env.NODE_ENV || 'development';
var express = require('express');
var app = express();
require("node-jsx").install();
var config = require('./config').items;
var React = require('react');
var routes = require('./routes')(app);
var ejs = require('ejs');



app.set('view engine', 'ejs');
app.use(express.static('../src'));

var server = app.listen(config[env].port, function () {
  console.log('listening at http://%s:%s', config[env].host, config[env].port);
});


