var React = require('react');
var ReactDOM = require('react-dom');



var MyComponent = React.createClass({

	componentDidMount : function() {

		var coords;
		var locationReady = this;

		//get html5 geo location
		if (navigator.geolocation) {

			navigator.geolocation.getCurrentPosition(function(e) {

				coords = {
					  lat : e.coords.latitude,
					  lon : e.coords.longitude
				};

				locationReady.setState({geoResult: coords.lat})

				//send results server
				

			}, function(error) {

				if (error.PERMISSION_DENIED) {
					locationReady.setState({geoResult: 'nope to geo'});
					//show alternate experience (zip code entry)
				}

				if (error.POSITION_UNAVAILABLE) {
					locationReady.setState({geoResult: 'postion unavailable'});
					//show alternate experience (zip code entry)
				}

				if (error.UNKNOWN_ERROR) {
					locationReady.setState({geoResult: 'unknown error'});
					//show alternate experience (zip code entry)
				}

			});

		}
		
	},
	
	getInitialState: function() {
      return {
          geoResult: null
      }
   },


  render: function() {
		return <div>{this.state.geoResult}</div>;
  }
});

ReactDOM.render(<MyComponent />, document.getElementById('container'));