var path = require('path');
var config = {
    entry: path.resolve(__dirname, 'src/js/main.js'),
    output: {
        path: path.resolve(__dirname, 'src/js'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            loader: 'babel',
            include : path.resolve(__dirname, 'src/js'),
            query:
            {
                presets:['es2015', 'react']
            }
        }]
    },

};

module.exports = config;
